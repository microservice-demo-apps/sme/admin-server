FROM openjdk:8-jdk-alpine
LABEL maintainer="ahmedbaz1024"
WORKDIR /usr/local/bin/
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} admin-server-1.0.jar
EXPOSE 9090
CMD ["java","-jar","admin-server-1.0.jar"]
